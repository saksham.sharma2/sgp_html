<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section top-space" style="background-image: url('images/breadcame.png');">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>FEEDBACK</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->

<section class="feedback_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="feedback_inner">
                    <h2>
                        We would like your feedback <br>to improve our website
                    </h2>
                    <div class="feed_back_form">
                        <form action="">
                            <div class="row">
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">How much you liked our website?</label>
                                        <div class="rated">
                                        <select class="star-rating">
                                            <option value=""></option>
                                            <option value="5"></option>
                                            <option value="4"></option>
                                            <option value="3"></option>
                                            <option value="2"></option>
                                            <option value="1"></option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">Please select your feedback category below</label>
                                        <div class="selector">
                                            <div class="selecotr-item">
                                                <input type="radio" id="radio1" name="selector" class="selector-item_radio" checked>
                                                <label for="radio1" class="selector-item_label">Complaint</label>
                                            </div>
                                            <div class="selecotr-item">
                                                <input type="radio" id="radio2" name="selector" class="selector-item_radio">
                                                <label for="radio2" class="selector-item_label">Suggestion</label>
                                            </div>
                                            <div class="selecotr-item">
                                                <input type="radio" id="radio3" name="selector" class="selector-item_radio">
                                                <label for="radio3" class="selector-item_label">Compliment</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">Email Address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter your email address" aria-describedby="emailHelp">
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">Please leave your feedback below</label>
                                        <textarea name="" class="form-control" placeholder="Write your feedback "></textarea>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="submit_bt float-end">
                                        <button type="submit" class="btn btn-primary-1">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>