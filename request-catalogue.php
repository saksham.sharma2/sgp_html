<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section top-space" style="background-image: url('images/breadcame.png');">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>REQUEST A CATALOGUE</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->

<section class="request-log-section body_inner">
    <div class="container">
        <div class="row">
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
              <div class="sample_slide">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="form-label">#  Sample Number</label>
                        <input type="number" class="form-control" id="sample-number" placeholder="Enter sample number">
                    </div>
                </form>
              </div>
                <div class="sample_slider_area">
                    <div class="owl-carousel owl-theme" id="sample-item">
                        <div class="item">
                            <div class="sample_inner_slider">
                                <div class="img_area">
                                    <img src="images/Layer1.png" alt=".." />
                                </div>
                                <div class="content">
                                    <h3>
                                        Black Front Logo Green Piping
                                    </h3>
                                    <p>
                                        Jersey - sample design 2335
                                    </p>
                                </div>
                                <div class="sample-number">
                                    <p>
                                        #2335
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="sample_inner_slider">
                                <div class="img_area">
                                    <img src="images/Layer1.png" alt=".." />
                                </div>
                                <div class="content">
                                    <h3>
                                        Black Front Logo Green Piping
                                    </h3>
                                    <p>
                                        Jersey - sample design 2335
                                    </p>
                                </div>
                                <div class="sample-number">
                                    <p>
                                        #2335
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="sample_inner_slider">
                                <div class="img_area">
                                    <img src="images/Layer1.png" alt=".." />
                                </div>
                                <div class="content">
                                    <h3>
                                        Black Front Logo Green Piping
                                    </h3>
                                    <p>
                                        Jersey - sample design 2335
                                    </p>
                                </div>
                                <div class="sample-number">
                                    <p>
                                        #2335
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                <div class="query">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="order-tab" data-bs-toggle="tab" data-bs-target="#order" type="button" role="tab" aria-controls="order" aria-selected="true">Order</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="query-tab" data-bs-toggle="tab" data-bs-target="#query" type="button" role="tab" aria-controls="query" aria-selected="false">Query</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="order" role="tabpanel" aria-labelledby="order-tab">
                            <div class="order-form-details">
                                <form class="row">
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""> <img src="images/user1.png"  alt="..." /> First Name</label>
                                            <input type="text" class="form-control" id="name" placeholder="Enter your first name">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/user1.png"  alt="..." />Last Name</label>
                                            <input type="text" class="form-control" id="lname" placeholder="Enter your Last name">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/atemail.png"  alt="..." />Email</label>
                                            <input type="email" class="form-control" id="eamil" placeholder=" Enter your email address">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/smartphone.png"  alt="..." />Cell number</label>
                                            <input type="number" class="form-control" id="cell" placeholder=" Enter your cell number">
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/location1.png"  alt="..." />Address</label>
                                            <input type="text" class="form-control" id="address" placeholder="Enter your address">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/location1.png"  alt="..." />City</label>
                                            <input type="text" class="form-control" id="city" placeholder="Enter your city">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/pin1.png"  alt="..." />Zip/Postal code</label>
                                            <input type="number" class="form-control" id="zip-code" placeholder="Zip/Postal code">
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/mail.png"  alt="..." />Message</label>
                                            <textarea name="" class="form-control" placeholder="Write your message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="subiter">
                                            <button type="submit" class="btn btn-primary-1">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="query" role="tabpanel" aria-labelledby="query-tab">
                            <div class="order-form-details">
                                <form class="row">
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""> <img src="images/user1.png"  alt="..." /> First Name</label>
                                            <input type="text" class="form-control" id="name" placeholder="Enter your first name">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/user1.png"  alt="..." />Last Name</label>
                                            <input type="text" class="form-control" id="lname" placeholder="Enter your Last name">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/atemail.png"  alt="..." />Email</label>
                                            <input type="email" class="form-control" id="eamil" placeholder=" Enter your email address">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/smartphone.png"  alt="..." />Cell number</label>
                                            <input type="number" class="form-control" id="cell" placeholder=" Enter your cell number">
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/location1.png"  alt="..." />Address</label>
                                            <input type="text" class="form-control" id="address" placeholder="Enter your address">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/location1.png"  alt="..." />City</label>
                                            <input type="text" class="form-control" id="city" placeholder="Enter your city">
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/pin1.png"  alt="..." />Zip/Postal code</label>
                                            <input type="number" class="form-control" id="zip-code" placeholder="Zip/Postal code">
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for=""><img src="images/mail.png"  alt="..." />Message</label>
                                            <textarea name="" class="form-control" placeholder="Write your message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="subiter">
                                            <button type="submit" class="btn btn-primary-1">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>