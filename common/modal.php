
<!--forget password Modal -->
<div class="modal fade" id="forget-password"  data-bs-backdrop="static" data-bs-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h5 class="modal-title" id="exampleModalLabel">Forgot Password</h5>
        <p>Enter your registered email we'll send you a link to reset your password.</p>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
         <div class="form-body">
             <form action="">
                 <div class="form-group">
                     <label for="">
                         Email
                     </label>
                     <input type="text" class="form-control" name="" placeholder="Enter your email">
                 </div>
             </form>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary w-100">Send link</button>
      </div>
    </div>
  </div>
</div>