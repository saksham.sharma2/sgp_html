<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SGP HTML</title>
	<!-- ==== Bootstrap CSS ==== -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/css/bootstrap.min.css" integrity="sha512-GQGU0fMMi238uA+a/bdWJfpUGKUkBdgfFdgBm72SUQ6BeyWjoY/ton0tEjH+OSH9iP4Dfh+7HM0I9f5eR0L/4w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- ==== Poppins Fonts ==== -->
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Syncopate:wght@400;700&display=swap" rel="stylesheet">
	<!-- ==== Font Awesome CSS ==== -->
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.4/css/all.css" />
	<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
	<link rel='stylesheet' href='https://cdn-uicons.flaticon.com/uicons-regular-rounded/css/uicons-regular-rounded.css'>
	<link rel='stylesheet' href='https://cdn-uicons.flaticon.com/uicons-solid-rounded/css/uicons-solid-rounded.css'>
	
	
	<!-- ==== Owl Carausel CSS ==== -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!--=== Fancy box css === -->
    
	<!-- ==== Custom CSS ==== -->
	<link rel="stylesheet" href="css/star-rating.min.css" />
	<link rel="stylesheet" href="css/custom.css" />
	
</head>
<body>
<?php 
        if($_SERVER['HTTP_HOST'] == 'localhost')
        {
			$url = 'http://localhost/html/sgp_html/';
        }
        else
        {
            $url = '/';
        }
        $headerClass = explode('/', $_SERVER['REQUEST_URI']);
        $headerClass = array_filter($headerClass);
        $headerClass = array_values($headerClass);
        $headerClass = end($headerClass);
    ?>

<section class="header-main-section">
	<div class="header_menu_wraper">
		<div class="container-fluid">
			<div class="row align-items-center align-items-end">
				<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 align-items-center align-items-end">
					<div class="head_navigation">
						<div class="left">
							<div class="logo">
								<div class="img_logo">
									<a href="index.php">
									<img src="images/ZYYM-Logo-1.png" alt="..." />
									</a>
								</div>
							</div>
							<div class="navgiation">
								<a href="javascript:;" class="open">
									<img src="images/Icon.png" alt="..." />
								</a>
								<a href="javascript:;" class="close d-none">
									<i class="fal fa-times"></i>
								</a>
							</div>
						</div>
						<div class="right">
							<div class="search">
								<form>
									<div class="seracher">
										<input type="text" class="form-control" id="serach" aria-describedby="" placeholder="Search">
										<div class="icon_wrap">
											<a href="javascript:;">
												<i class="far fa-search"></i>
											</a>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="menu_list d-none">
							<ul class="">
								<li class="menu_dropdown">
									<a href="javascript:;" class="">
										<div class="img_box">
											<img src="images/uniform1.png" alt=".." />
										</div>
										<div class="dress_name">
											 <p>Military Uniform</p>
										</div>
									</a>
									<ul class="sub_menu_drop_uniform">
										<li class="sub_menu_item">
                                           <a href="javascript:;" class="toogle_menu">
											   <p>Uniform</p>
											   <i class="far fa-angle-down"></i>
										   </a>
										   	<ul class="sub_menu">
											   <li>
                                                   <a href="listing.php">
													   <p>Jerseys</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													  <p> Jackets</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Shirts</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Trousers</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Shoes</p>
												   </a>
											   </li>
										   	</ul>
										</li>
										<li class="sub_menu_item">
                                           <a href="javascript:;" class="toogle_menu">
											   <p>Headwear</p>
											   <i class="far fa-angle-down"></i>
										   </a>
										   	<ul class="sub_menu">
											   <li>
                                                   <a href="listing.php">
													   <p>Jerseys</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													  <p> Jackets</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Shirts</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Trousers</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Shoes</p>
												   </a>
											   </li>
										   	</ul>
										</li>
										<li class="sub_menu_item">
                                           <a href="javascript:;" class="toogle_menu">
											   <p>Accessory</p>
											   <i class="far fa-angle-down"></i>
										   </a>
										   	<ul class="sub_menu">
											   <li>
                                                   <a href="listing.php">
													   <p>Jerseys</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													  <p> Jackets</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Shirts</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Trousers</p>
												   </a>
											   </li>
											   <li>
                                                   <a href="listing.php">
													   <p>Shoes</p>
												   </a>
											   </li>
										   	</ul>
										</li>
									</ul>
								</li>
								<li>
									<a href="javascript:;">
										<div class="img_box">
											<img src="images/long-sleeve.png" alt=".." />
										</div>
										<div class="dress_name">
											<p>School Uniform</p>
										</div>
									</a>
									<ul class="sub_menu_other">
										<li>
											<a href="listing.php">
												<p>
													Shirts
												</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>
													Shirts
												</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>
													Shirts
												</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>
													Shirts
												</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>
													Shirts
												</p>
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="javascript:;">
										<div class="img_box">
											<img src="images/t-shirt.png" alt=".." />
										</div>
										<div class="dress_name">
											<p>Sports Wear</p>
										</div>
									</a>
									<ul class="sub_menu_other">
										<li>
											<a href="listing.php">
												<p>Jerseys</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p> Jackets</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Shirts</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Trousers</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Shoes</p>
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="javascript:;">
										<div class="img_box">
											<img src="images/scarf.png" alt=".." />
										</div>
										<div class="dress_name">
											<p>Scarves & Mufflers</p>
										</div>
									</a>
									<ul class="sub_menu_other">
										<li>
											<a href="listing.php">
												<p>Jerseys</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p> Jackets</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Shirts</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Trousers</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Shoes</p>
											</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="javascript:;">
										<div class="img_box">
											<img src="images/prayer-mat.png" alt=".." />
										</div>
										<div class="dress_name">
											<p>Rugs & Throws</p>
										</div>
									</a>
									<ul class="sub_menu_other">
										<li>
											<a href="listing.php">
												<p>Jerseys</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p> Jackets</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Shirts</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Trousers</p>
											</a>
										</li>
										<li>
											<a href="listing.php">
												<p>Shoes</p>
											</a>
										</li>
									</ul>	
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>