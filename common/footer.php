<footer>
	<div class="container">
		<div class="row">
			<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="news_area_wrap">
					<div class="news_head text-center">
						<h2>
							newsletter
						</h2>
						<p>
							Be the first one to know  about discounts, offers and events weekly in your mailbox. Unsubscribe whenever you like with one click.
						</p>
					</div>
					<div class="news_searcher">
						<form>
							<div class="seracher">
								<input type="email" class="form-control" id="email" aria-describedby="" placeholder="Enter your email">
								<div class="icon_wrap">
									<a href="javascript:;" class="btn btn-primary-1">
										Submit
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="footer_area">
					<div class="row">
						<div class="col-xxl-5 col-xl-5 col-lg-5 col-md-5 col-sm-5 col-12">
							<div class="foot-about-side">
								<div class="logo_img">
									<a href="index.php">
										<img src="images/Layer 2.png" alt="..." />
									</a>	
								</div>
								<div class="description">
									<p>
									SGP & COMPANY was raised in the early 1960s by S.Sajjan Singh, who had wealth of experience in this field.
									</p>
								</div>
								<div class="social">
									<ul>
										<li>
											<a href="javascript:;">
												<i class="fab fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<i class="fab fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<i class="fab fa-instagram"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xxl-7 col-xl-7 col-lg-7 col-md-7 col-sm-7 col-12">
							<div class="row">
								<div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
									<div class="one-forth">
										<div class="title">
											<h2>categories</h2>
										</div>
										<div class="footer-menu">
											<ul>
												<li>
													<a href="listing.php">
														<p>Military Uniform</p>
													</a>
												</li>
												<li>
													<a href="listing.php">
														<p>School Uniform</p>
													</a>
												</li>
												<li>
													<a href="listing.php">
														<p>Sports Wear</p>
													</a>
												</li>
												<li>
													<a href="listing.php">
														<p>Scarves & Mufflers</p>
													</a>
												</li>
												<li>
													<a href="listing.php">
														<p>Rugs & Throws</p>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
									<div class="one-forth">
										<div class="title">
											<h2>Company</h2>
										</div>
										<div class="footer-menu">
											<ul>
												<li>
													<a href="index.php">
														<p>Home</p>
													</a>
												</li>
												<li>
													<a href="about_us.php">
														<p>About Us</p>
													</a>
												</li>
												<li>
													<a href="feedback.php">
														<p>Feedback</p>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
									<div class="one-forth">
										<div class="title">
											<h2>Help </h2>
										</div>
										<div class="footer-menu">
											<ul>
												<li>
													<a href="contact_us.php">
														<p>Contact Us</p></a>
												</li>
												<li>
													<a href="faq.php">
														<p>FAQs</p>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="copy-right">
					<div class="text-center">
						<p>© &nbsp;Copyright 2022 &nbsp;|&nbsp; All Rights Reserved &nbsp;|&nbsp; Powered By <a href="https://globiztechnology.com/" target="_blank">Globiz Technology Inc.</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php include('common/modal.php') ?>

	<!-- ==== jQuery JS ==== -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<!-- ==== Bootstrap JS ==== -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
	<!-- ==== Owl Carausel Js ==== -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
	<!-- ==== jQuery validation JS ==== -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
	<!-- ==== jQuery Additional method validation JS ==== -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js"></script>
	<!-- ==== jQuery matchHeight JS === -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
	<!-- ==== Fancy box JS === -->
	<!-- ==== Custom js ==== -->
	<script type="text/javascript" src="js/custom.js"></script>
	<script src="js/star-rating.js"></script>
	<script>
		var stars = new StarRating('.star-rating');
	</script>
</body>

</html>