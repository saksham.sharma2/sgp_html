<!-- ==== Add audio ==== -->
<div class="modal el_modal uploadFile fade" id="uploadaudio" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add audio</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="far fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <form action="" method="post">
                        <div class="form-group">
                            <label class="el_label">Title</label>
                            <input type="text" class="form-control" name="" placeholder="Title" />
                        </div>
                        <div class="form-group">
                            <label class="el_label">Type</label>
                            <div class="custom-control-radio custom-radio">
                                <input class="form-check-input" type="radio" name="where" id="r-1" value="1" checked="">
                                <label class="form-check-label" for="r-1">
                                Free
                                </label>
                                <input class="form-check-input" type="radio" name="where" id="r-2" value="1" >
                                <label class="form-check-label" for="r-2">
                                Paid
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="el_label">Upload audio</label>
                            <div class="file-upload">
                                <label class="file-input" for="file-input">
                                    <img src="images/cloud-upload1.png" alt="..." />
                                    <p>Select your audio to upload</p>
                                </label>
                                <input id="file-input" class="d-none" type="file"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="left_btn">
                    <button type="button" class="btn btn-primary-2" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="right_btn">
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== Add Videos ==== -->
<div class="modal el_modal uploadFile fade" id="uploadvideo" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add video</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="far fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <form action="" method="post">
                        <div class="form-group">
                            <label class="el_label">Title</label>
                            <input type="text" class="form-control" name="" placeholder="Title" />
                        </div>
                        <div class="form-group">
                            <label class="el_label">Type</label>
                            <div class="custom-control-radio custom-radio">
                                <input class="form-check-input" type="radio" name="wheres" id="r-3" value="1" checked="">
                                <label class="form-check-label" for="r-3">
                                Free
                                </label>
                                <input class="form-check-input" type="radio" name="wheres" id="r-4" value="1" >
                                <label class="form-check-label" for="r-4">
                                Paid
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="el_label">Upload video</label>
                            <div class="file-upload">
                                <label class="file-input" for="file-input">
                                    <img src="images/cloud-upload1.png" alt="..." />
                                    <p>Select your video to upload</p>
                                </label>
                                <input id="file-input" class="d-none" type="file"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="left_btn">
                    <button type="button" class="btn btn-primary-2" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="right_btn">
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== Add announcement ==== -->
<div class="modal el_modal uploadFile fade" id="add-announcement" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add announcement</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="far fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <form action="" method="post">
                        <div class="form-group">
                            <label class="el_label">Title</label>
                            <input type="text" class="form-control" name="" placeholder="Title" />
                        </div>
                        <div class="form-group">
                            <label class="el_label">Upload file</label>
                            <div class="file-upload">
                                <label class="file-input" for="file-input">
                                    <img src="images/cloud-upload1.png" alt="..." />
                                    <p>Drop your file here to upload</p>
                                </label>
                                <input id="file-input" class="d-none" type="file"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="left_btn">
                    <button type="button" class="btn btn-primary-2" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="right_btn">
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== set up meeting ==== -->
<div class="modal el_modal uploadFile fade" id="set-meet" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Set up Meeting</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="far fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12  col-md-12 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label class="el_label">Meeting topic</label>
                                    <input type="text" class="form-control" name="" placeholder="Meeting topic" />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6  col-md-6 col-sm-6 col-12 ">
                                <div class="form-group">
                                    <label class="el_label">Date</label>
                                    <div class="input-group" id="start_date_box">
                                        <input type="text" class="form-control" name="start_date" id="start_date" placeholder="Date">
                                        <div class="icon">
                                            <i class="fi fi-rr-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6  col-md-6 col-sm-6 col-12 ">
                                <div class="form-group">
                                    <label class="el_label">Time</label>
                                    <select id="inputcategory" class="form-select">
                                        <option selected="">Time</option>
                                        <option>..</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12  col-md-12 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label class="el_label">Participants</label>
                                    <input type="text" class="form-control" name="" placeholder="Enter no. of participants " />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12  col-md-12 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label class="el_label">Meeting link</label>
                                    <input type="text" class="form-control" name="" placeholder="Enter meeting link" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="left_btn">
                    <button type="button" class="btn btn-primary-2" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="right_btn">
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== set up meeting ==== -->
<div class="modal el_modal uploadFile fade" id="schdule" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Schdule</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="far fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <form action="" method="post">
                        <div class="row">
                            
                            <div class="col-xxl-12 col-xl-12 col-lg-12  col-md-12 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label class="el_label">Date</label>
                                    <div class="input-group" id="new_date_box">
                                        <input type="text" class="form-control" name="new_date" id="new_date" placeholder="Date">
                                        <div class="icon">
                                            <i class="fi fi-rr-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12  col-md-12 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label class="el_label">Title</label>
                                    <input type="text" class="form-control" name="" placeholder="Enter your title" />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12  col-md-12 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label class="el_label">Description</label>
                                    <textarea name="" rows="5" class="form-control" placeholder="Description"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="left_btn">
                    <button type="button" class="btn btn-primary-2" data-bs-dismiss="modal">Cancel</button>
                </div>
                <div class="right_btn">
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>