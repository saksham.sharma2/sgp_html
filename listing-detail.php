<!-- ==== Header === -->
<?php include('common/header.php') ?>


<!-- ==== Listing Section ==== -->
<div class="body_inner Listing_detail_main ">
    <section class="Listing_detail_section top-space">
        <div class="listing_divide">
            <div class="listing_left_side">
                <div class="img_area">
                    <img src="images/Layer1.png" alt="">
                </div>
            </div>
            <div class="listing_right_side">
                <div class="content">
                    <h2>
                        Blue jersey Lorem
                    </h2>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis neque orci, fringilla ut justo at, aliquam rutrum turpis. Quisque eu vehicula justo. Nulla pretium egestas ligula, et convallis enim egestas at.
                    </p>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nequet convallis enim egestas at.
                    </p>
                </div>
                <div class="contact_butn">
                    <a href="request-catalogue.php" class="btn btn-primary-1">Contact</a>
                    <a href="javascript:;" class="btn btn-primary-3">Share</a>
                </div>
            </div>
        </div>
    </section>

    <section class="combination_product">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="combat_head">
                        <h2>
                            Combination Products
                        </h2>
                    </div>
                    <div class="comat-slider">
                        <div class="arrvials_slider-wrap">
                            <div class="owl-carousel owl-theme" id="arrvials">
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_QNyRp21hb5I.png" alt="..." />
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_0IsBu45B3T8.png" alt="..." />                             
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_mhi10r2L0fA.png" alt="..." />
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_s3AFTBZ3cnc.png" alt="..." />
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_mhi10r2L0fA.png" alt="..." />
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="combination_product">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="combat_head">
                        <h2>
                            Pairs
                        </h2>
                    </div>
                    <div class="comat-slider">
                        <div class="arrvials_slider-wrap">
                            <div class="owl-carousel owl-theme" id="pairs">
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_QNyRp21hb5I.png" alt="..." />
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_0IsBu45B3T8.png" alt="..." />                             
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_mhi10r2L0fA.png" alt="..." />
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_s3AFTBZ3cnc.png" alt="..." />
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="arrvials_slider">
                                        <a href="listing-detail.php">
                                            <div class="arvial-slide-img">
                                                <img src="images/unsplash_mhi10r2L0fA.png" alt="..." />
                                            </div>
                                        </a>
                                        <div class="description">
                                            <a href="listing-detail.php">
                                                <h6>
                                                    Pink Lorem sit
                                                </h6>
                                            </a>
                                            <p>
                                                Lorem ipsum sit sor dor
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>