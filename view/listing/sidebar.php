<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-5">
    <div class="sidebar_rows_area">
        <div class="search_box">
            <form action="" method="get">
                <div class="form-group">
                    <input type="text" class="form-control" name="search" placeholder="Search"/>
                    <button type="button" class="btn btn-default">
                        <i class="far fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
        <div class="sidebar_filters_area">
            <div class="top_heading d-sm-none b-block">
                    <a href="javascript:;" class="open_filter">
                     <h5>Filters</h5>
                        <i class="fas fa-filter"></i>
                    </a>
                </div>
            <div class="inner_filters_area">
                <div class="top_heading d-md-block d-none">
                    <h2>CATEGORY</h2>
                    <a href="javascript:;" class="open_filter">
                    </a>
                </div>
                <div class="scroable_area">
                    <div class="top_heading d-md-none d-block">
                        <h2>CATEGORY</h2>
                        <a href="javascript:;" class="open_filter">
                        </a>
                    </div>
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                                <div class="cross_row">
                                    <a href="javascript:;" class="cross_filter">
                                        <img src="images/cross.png" alt="..." />
                                    </a>
                                </div>
                            <div class="filter_box district_box">
                                <h2 class="accordion-header" id="headingone">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseone" aria-expanded="false" aria-controls="collapseone">
                                        <div class="filter_title">
                                            <h5>Military Uniform</h5>
                                        </div>
                                    </button>
                                </h2>
                                <div id="collapseone" class="accordion-collapse collapse" aria-labelledby="headingone" data-bs-parent="#accordionExample-1">
                                    <div class="accordion" id="accordionExample-1">
                                        <div class="accordion-body sub">
                                            <div class="accordion-item">
                                                <div class="filter_box district_box">
                                                    <div class="cross_row">
                                                        <a href="javascript:;" class="cross_filter">
                                                            <img src="images/cross (1).png" alt="..." />
                                                        </a>
                                                    </div>
                                                    <h2 class="accordion-header" id="headingtwo">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsetwo" aria-expanded="false" aria-controls="collapsetwo">
                                                            <div class="filter_title">
                                                                <h5>Uniforms</h5>
                                                            </div>
                                                        </button>
                                                    </h2>
                                                    <div id="collapsetwo" class="accordion-collapse collapse" aria-labelledby="headingtwo" data-bs-parent="#accordionExample-1">
                                                        <div class="accordion-body">
                                                            <div class="check_row">
                                                                <ul>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="1-1" name="accessibility[]" value="1">
                                                                            <label class="custom-control-label" for="1-1">Jerseys</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="1-2" name="accessibility[]" value="2">
                                                                            <label class="custom-control-label" for="1-2">Jackets</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="1-3" name="accessibility[]" value="3">
                                                                            <label class="custom-control-label" for="1-3">Shirts</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="1-4" name="accessibility[]" value="4">
                                                                            <label class="custom-control-label" for="1-4">Trousers</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="1-5" name="accessibility[]" value="5">
                                                                            <label class="custom-control-label" for="1-5">Shoes</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-item">
                                                <div class="filter_box district_box">
                                                    <h2 class="accordion-header" id="headingthree">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsethree" aria-expanded="false" aria-controls="collapsethree">
                                                            <div class="filter_title">
                                                                <h5>Headware</h5>
                                                            </div>
                                                        </button>
                                                    </h2>
                                                    <div id="collapsethree" class="accordion-collapse collapse" aria-labelledby="headingthree" data-bs-parent="#accordionExample-1">
                                                        <div class="accordion-body">
                                                            <div class="check_row">
                                                                <ul>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="2-1" name="accessibility[]" value="1">
                                                                            <label class="custom-control-label" for="2-1">Jerseys</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="2-2" name="accessibility[]" value="2">
                                                                            <label class="custom-control-label" for="2-2">Jackets</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="2-3" name="accessibility[]" value="3">
                                                                            <label class="custom-control-label" for="2-3">Shirts</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="2-4" name="accessibility[]" value="4">
                                                                            <label class="custom-control-label" for="2-4">Trousers</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="2-5" name="accessibility[]" value="5">
                                                                            <label class="custom-control-label" for="2-5">Shoes</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-item">
                                                <div class="filter_box district_box">
                                                    <h2 class="accordion-header" id="headingfour">
                                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                            <div class="filter_title">
                                                                <h5>Accesssories</h5>
                                                            </div>
                                                        </button>
                                                    </h2>
                                                    <div id="collapsefour" class="accordion-collapse collapse" aria-labelledby="headingfour" data-bs-parent="#accordionExample-1">
                                                        <div class="accordion-body">
                                                            <div class="check_row">
                                                                <ul>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="3-1" name="accessibility[]" value="1">
                                                                            <label class="custom-control-label" for="3-1">Jerseys</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="3-2" name="accessibility[]" value="2">
                                                                            <label class="custom-control-label" for="3-2">Jackets</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="3-3" name="accessibility[]" value="3">
                                                                            <label class="custom-control-label" for="3-3">Shirts</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="3-4" name="accessibility[]" value="4" checked>
                                                                            <label class="custom-control-label" for="3-4">Trousers</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="3-5" name="accessibility[]" value="5">
                                                                            <label class="custom-control-label" for="3-5">Shoes</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="filter_box district_box">
                                <h2 class="accordion-header" id="headingfive">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                        <div class="filter_title">
                                            <h5>School Uniform</h5>
                                        </div>
                                    </button>
                                </h2>
                                <div id="collapsefive" class="accordion-collapse collapse" aria-labelledby="headingfive" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="check_row">
                                            <ul>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="4-1" name="accessibility[]" value="1">
                                                        <label class="custom-control-label" for="4-1">Jerseys</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="4-2" name="accessibility[]" value="2">
                                                        <label class="custom-control-label" for="4-2">Jackets</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="4-3" name="accessibility[]" value="3">
                                                        <label class="custom-control-label" for="4-3">Shirts</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="4-4" name="accessibility[]" value="4" checked>
                                                        <label class="custom-control-label" for="4-4">Trousers</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="4-5" name="accessibility[]" value="5">
                                                        <label class="custom-control-label" for="4-5">Shoes</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="filter_box district_box">
                                <h2 class="accordion-header" id="headingsix">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                        <div class="filter_title">
                                            <h5>Sports Wear</h5>
                                        </div>
                                    </button>
                                </h2>
                                <div id="collapsesix" class="accordion-collapse collapse" aria-labelledby="headingsix" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="check_row">
                                            <ul>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="5-1" name="accessibility[]" value="1">
                                                        <label class="custom-control-label" for="5-1">Jerseys</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="5-2" name="accessibility[]" value="2">
                                                        <label class="custom-control-label" for="5-2">Jackets</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="5-3" name="accessibility[]" value="3">
                                                        <label class="custom-control-label" for="5-3">Shirts</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="5-4" name="accessibility[]" value="5" checked>
                                                        <label class="custom-control-label" for="5-4">Trousers</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="5-5" name="accessibility[]" value="5">
                                                        <label class="custom-control-label" for="5-5">Shoes</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="filter_box district_box">
                                <h2 class="accordion-header" id="headingseven">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
                                        <div class="filter_title">
                                            <h5>Scarves & Mufflers</h5>
                                        </div>
                                    </button>
                                </h2>
                                <div id="collapseseven" class="accordion-collapse collapse" aria-labelledby="headingseven" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="check_row">
                                            <ul>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="6-1" name="accessibility[]" value="1">
                                                        <label class="custom-control-label" for="6-1">Jerseys</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="6-2" name="accessibility[]" value="2">
                                                        <label class="custom-control-label" for="6-2">Jackets</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="6-3" name="accessibility[]" value="3">
                                                        <label class="custom-control-label" for="6-3">Shirts</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="6-4" name="accessibility[]" value="4" checked>
                                                        <label class="custom-control-label" for="6-4">Trousers</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="6-5" name="accessibility[]" value="5">
                                                        <label class="custom-control-label" for="6-5">Shoes</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="filter_box district_box">
                                <h2 class="accordion-header" id="headingeight">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
                                        <div class="filter_title">
                                            <h5>Rugs & Throws</h5>
                                        </div>
                                    </button>
                                </h2>
                                <div id="collapseeight" class="accordion-collapse collapse" aria-labelledby="headingeight" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="check_row">
                                            <ul>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="7-1" name="accessibility[]" value="1">
                                                        <label class="custom-control-label" for="7-1">Jerseys</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="7-2" name="accessibility[]" value="2">
                                                        <label class="custom-control-label" for="7-2">Jackets</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="7-3" name="accessibility[]" value="3">
                                                        <label class="custom-control-label" for="7-3">Shirts</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="7-4" name="accessibility[]" value="4" checked>
                                                        <label class="custom-control-label" for="7-4">Trousers</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="7-5" name="accessibility[]" value="5">
                                                        <label class="custom-control-label" for="7-5">Shoes</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sort_by">
                            <div class="top_heading">
                                <h2>SORT BY</h2>
                            </div>
                            <div class="sort_inner">
                                <div class="check_row">
                                    <ul>
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="8-1" name="accessibility[]" value="1">
                                                <label class="custom-control-label" for="8-1">Popular</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="8-2" name="accessibility[]" value="2">
                                                <label class="custom-control-label" for="8-2">A - Z</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="8-3" name="accessibility[]" value="3">
                                                <label class="custom-control-label" for="8-3">Z - A</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>