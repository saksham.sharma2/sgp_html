<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="banner_section top-space">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  p-0">
                <div class="banner_slide_wrap">
                    <div class="owl-carousel owl-theme" id="banner_slide">
                        <div class="item">
                            <div class="banner_slider">
                                <div class="banner_img">
                                    <img src="images/Vector.png" alt="..." />
                                </div>
                                <div class="content">
                                    <h2>
                                        uniform <span>&</span> accessories
                                    </h2>
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. 
                                    </p>
                                    <div class="explore_more">
                                        <a href="listing.php" class="btn btn-primary">
                                            Explore More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="banner_slider">
                                <div class="banner_img">
                                    <img src="images/Vector.png" alt="..." />
                                </div>
                                <div class="content">
                                    <h2>
                                        uniform <span>&</span> accessories
                                    </h2>
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. 
                                    </p>
                                    <div class="explore_more">
                                        <a href="listing.php" class="btn btn-primary">
                                            Explore More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="banner_slider">
                                <div class="banner_img">
                                    <img src="images/Vector.png" alt="..." />
                                </div>
                                <div class="content">
                                    <h2>
                                        uniform <span>&</span> accessories
                                    </h2>
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. 
                                    </p>
                                    <div class="explore_more">
                                        <a href="listing.php" class="btn btn-primary">
                                            Explore More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="uniform_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="uniform_view">
                    <div class="unfi_head d-md-block d-none">
                        <h1>
                            Uniforms
                        </h1>
                    </div>
                    <div class="uniform_img_box_area">
                        <img src="images/army.png" alt="..." />
                    </div>
                    <div class="dress_category">
                        <div class="unfi_head d-md-none d-block">
                            <h1>
                                Uniforms
                            </h1>
                        </div>
                        <div class="">
                            <ul>
                                <li class="">
                                    <a href="listing.php">
                                        SHIRTS
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                        TROUSERS
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                    JACKETS
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                        JERSEY
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                        SHOES
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="uniform_section school">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="uniform_view">
                    <div class="dress_category">
                        <div class="unfi_head d-md-none d-block">
                            <h1>
                                SCHOOL<br>UNIFORMS
                            </h1>
                        </div>
                        <div class="">
                            <ul>
                                <li class="">
                                    <a href="listing.php">
                                        SCHOOL SHIRTS
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                        T-SHIRTS
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                    TRACK SUITS
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                        BLAZERS & JACKETS
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="uniform_img_box_area">
                        <img src="images/school.png" alt="..." />
                    </div>
                    <div class="unfi_head d-md-block d-none">
                        <h1>
                            SCHOOL<br>UNIFORMS
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="uniform_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="uniform_view">
                    <div class="unfi_head d-md-block d-none">
                        <h1>
                        Sports <br>Wear
                        </h1>
                    </div>
                    <div class="uniform_img_box_area">
                        <img src="images/HOME.png" alt="..." />
                    </div>
                    <div class="dress_category">
                        <div class="unfi_head d-md-none d-block">
                            <h1>
                            Sports <br>Wear
                            </h1>
                        </div>
                        <div class="">
                            <ul>
                                <li class="">
                                    <a href="listing.php">
                                        T-SHIRTS
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                        TRACK SUITS
                                    </a>
                                </li>
                                <li>
                                    <a href="listing.php">
                                    SHORTS
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="new_arrivals_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="heading_area">
                    <h1>
                        New Arrivals
                    </h1>
                </div>
                <div class="arrvials_slider-wrap">
                    <div class="owl-carousel owl-theme" id="arrvials">
                        <div class="item">
                            <div class="arrvials_slider">
                                <a href="listing-detail.php">
                                    <div class="arvial-slide-img">
                                        <img src="images/unsplash_QNyRp21hb5I.png" alt="..." />
                                    </div>
                                </a>
                                <div class="description">
                                    <a href="listing-detail.php">
                                        <h6>
                                            Pink Lorem sit
                                        </h6>
                                    </a>
                                    <p>
                                        Lorem ipsum sit sor dor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="arrvials_slider">
                                <a href="listing-detail.php">
                                    <div class="arvial-slide-img">
                                        <img src="images/unsplash_0IsBu45B3T8.png" alt="..." />                             
                                    </div>
                                </a>
                                <div class="description">
                                    <a href="listing-detail.php">
                                        <h6>
                                            Pink Lorem sit
                                        </h6>
                                    </a>
                                    <p>
                                        Lorem ipsum sit sor dor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="arrvials_slider">
                                <a href="listing-detail.php">
                                    <div class="arvial-slide-img">
                                        <img src="images/unsplash_mhi10r2L0fA.png" alt="..." />
                                    </div>
                                </a>
                                <div class="description">
                                    <a href="listing-detail.php">
                                        <h6>
                                            Pink Lorem sit
                                        </h6>
                                    </a>
                                    <p>
                                        Lorem ipsum sit sor dor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="arrvials_slider">
                                <a href="listing-detail.php">
                                    <div class="arvial-slide-img">
                                        <img src="images/unsplash_s3AFTBZ3cnc.png" alt="..." />
                                    </div>
                                </a>
                                <div class="description">
                                    <a href="listing-detail.php">
                                        <h6>
                                            Pink Lorem sit
                                        </h6>
                                    </a>
                                    <p>
                                        Lorem ipsum sit sor dor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="arrvials_slider">
                                <a href="listing-detail.php">
                                    <div class="arvial-slide-img">
                                        <img src="images/unsplash_mhi10r2L0fA.png" alt="..." />
                                    </div>
                                </a>
                                <div class="description">
                                    <a href="listing-detail.php">
                                        <h6>
                                            Pink Lorem sit
                                        </h6>
                                    </a>
                                    <p>
                                        Lorem ipsum sit sor dor
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-us top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="about_head_area text-center">
                    <h2>
                        About Us
                    </h2>
                    <h1>
                        First You Should Know
                    </h1>
                </div>
                <div class="about_us_faces">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="about_us_items">
                                <div class="img_area">
                                    <img src="images/package-box.png" alt="..." />
                                </div>
                                <div class="content match_height_col">
                                    <h2>
                                        We Deliver
                                    </h2>
                                    <p>
                                        We supply wide range of uniform products used by Police, Armed forces and by many Government and non-government institutions.
                                    </p>
                                </div>
                                <div class="explore">
                                    <a href="404.php" class="btn btn-primary-1">Explore more <i class="fal fa-arrow-right"></i></a>
                                </div>
                            </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="about_us_items">
                                    <div class="img_area">
                                        <img src="images/hourglass.png" alt="..." />
                                    </div>
                                    <div class="content match_height_col">
                                        <h2>
                                            Our History
                                        </h2>
                                        <p>
                                            SGP & COMPANY was raised in the early 1960s by S.Sajjan Singh, who had wealth of experience in this field.
                                        </p>
                                    </div>
                                    <div class="explore">
                                        <a href="404.php" class="btn btn-primary-1">Explore more <i class="fal fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="about_us_items">
                                    <div class="img_area ">
                                        <img src="images/target.png" alt="..." />
                                    </div>
                                    <div class="content match_height_col">
                                        <h2>
                                            We Focus
                                        </h2>
                                        <p>
                                            Our goal is to provide the best value service, dependability and unsurpassed quality in the designs, fashion, fabrics and craftsmanship in uniform articles manufactured.
                                        </p>
                                    </div>
                                    <div class="explore">
                                        <a href="404.php" class="btn btn-primary-1">Explore more <i class="fal fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="about_head_area text-center">
                    <h2>
                        testimonials
                    </h2>
                    <h1>
                        What people say about us 
                    </h1>
                </div>
                <div class="testimonial_slider">
                    <div class="owl-carousel owl-theme" id="testimonial">
                        <div class="item">
                            <div class="slider_area">
                                <div class="rating">
                                    <ul>
                                        <li>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fal fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content match_height_col">
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam convallis libero elit, at rhoncus neque dignissim ac. Morbi suscipit nisl magna. Aliquam hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi suscipit nisl magna. Aliquam hendrerit.
                                    </p>
                                </div>
                                <div class="user_area">
                                    <div class="img_box">
                                        <img src="images/Rectangle32.png" alt="..." />
                                    </div>
                                    <div class="name">
                                        <h5>
                                            Jane Cooper
                                        </h5>
                                        <p>
                                            lorem ipsum
                                        </p>
                                        <div class="quto">
                                            <img src="images/quote-sign.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="slider_area">
                                <div class="rating">
                                    <ul>
                                        <li>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fal fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content match_height_col">
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                    </p>
                                </div>
                                <div class="user_area">
                                    <div class="img_box">
                                        <img src="images/Rectangle52.png" alt="..." />
                                    </div>
                                    <div class="name">
                                        <h5>
                                            Jane Cooper
                                        </h5>
                                        <p>
                                            lorem ipsum
                                        </p>
                                        <div class="quto">
                                            <img src="images/quote-sign.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="slider_area">
                                <div class="rating">
                                    <ul>
                                        <li>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fal fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content match_height_col">
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam convallis libero elit, at rhoncus neque dignissim ac. Morbi suscipit nisl magna. Aliquam hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi suscipit nisl magna. Aliquam hendrerit.
                                    </p>
                                </div>
                                <div class="user_area">
                                    <div class="img_box">
                                        <img src="images/Rectangle50.png" alt="..." />
                                    </div>
                                    <div class="name">
                                        <h5>
                                            Jane Cooper
                                        </h5>
                                        <p>
                                            lorem ipsum
                                        </p>
                                        <div class="quto">
                                            <img src="images/quote-sign.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="slider_area">
                                <div class="rating">
                                    <ul>
                                        <li>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fal fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content match_height_col">
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam convallis libero elit, at rhoncus neque dignissim ac. Morbi suscipit nisl magna. Aliquam hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi suscipit nisl magna. Aliquam hendrerit.
                                    </p>
                                </div>
                                <div class="user_area">
                                    <div class="img_box">
                                        <img src="images/Rectangle32.png" alt="..." />
                                    </div>
                                    <div class="name">
                                        <h5>
                                            Jane Cooper
                                        </h5>
                                        <p>
                                            lorem ipsum
                                        </p>
                                        <div class="quto">
                                            <img src="images/quote-sign.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="work_withus_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="work-with_us_inner">
                    <div class="row align-items-center">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="about_head_area pb-0">
                                <h2>
                                    Our Certifications
                                </h2>
                                <h1>
                                    Work with <br>experts
                                </h1>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="work-us-members-list">
                                <div class="row"> 
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="member-wrap">
                                            <div class="img-wrap">
                                                <img src="images/logo1.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="member-wrap">
                                            <div class="img-wrap">
                                                <img src="images/logo2.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="member-wrap">
                                            <div class="img-wrap">
                                                <img src="images/logo3.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="member-wrap">
                                            <div class="img-wrap">
                                                <img src="images/logo4.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="member-wrap">
                                            <div class="img-wrap">
                                                <img src="images/logo5.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="member-wrap">
                                            <div class="img-wrap">
                                                <img src="images/logo6.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== footer === -->
<?php include('common/footer.php') ?>