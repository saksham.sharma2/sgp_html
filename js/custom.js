
// Open Close resposnive menu
/* Open menu */
if($('.res_menubar').length)
{
$('body').on('click', '.res_menubar', function(){
    $(this).parents('.responsive_menu').find('.bar').addClass('hide_bar');
    $(this).parents('.responsive_menu').find('.open_menu').addClass('show_menu');
    $('body').addClass('scroll_off');
});
}
// Open Close resposnive menu
/* Open menu */
if($('.res_menubar').length)
{
$('body').on('click', '.res_menubar', function(){
    $(this).parents('.responsive_menu').find('.open_menu').addClass('show_open');
    $('body').addClass('scroll_off');
});
}

/* Close menu */
if($('.cross_menu a').length)
{
$('body').on('click', '.cross_menu a', function(){
    $(this).parents('.responsive_menu').find('.open_menu.show_open').removeClass('show_open');
   
});
}

if($('.open').length)
{
$('body').on('click', '.open', function(){
    $(this).parents('.head_navigation').find('.menu_list').removeClass('d-none');
    $(this).parents('.head_navigation').find('a.close').removeClass('d-none');
    $(this).parents('.head_navigation').find('a.open').addClass('d-none');
    $('body').addClass('scroll_off');
});
}


if($('.close').length)
{
$('body').on('click', '.close', function(){
    $(this).parents('.head_navigation').find('.menu_list').addClass('d-none');
    $(this).parents('.head_navigation').find('a.close').addClass('d-none');
    $(this).parents('.head_navigation').find('a.open').removeClass('d-none');
    $('body').removeClass('scroll_off');
});
}


$('body').on('click', '.toogle_menu', function(){
    that = $(this);
    if($(this).hasClass('active_menu'))
    {
        that.parent().find('.sub_menu').css({'display':'none'});
        that.removeClass('active_menu');
    }
    else
    {
        $('.sub_menu_drop_uniform .sub_menu_item .sub_menu').css({'display':'none'});
        $('.sub_menu_drop_uniform .sub_menu_item .toogle_menu').css({'color':'#757575'});
        $('.sub_menu_drop_uniform .sub_menu_item .toogle_menu').removeClass('active_menu');
        
        that.parent().find('.sub_menu').css({'display':'block'});
        that.addClass('active_menu');
        that.parent().find('.toogle_menu').css({'color':'#000'});
    }
});

// Mobile filter open

if($('.open_filter').length)
{
$('body').on('click', '.open_filter', function(){
    $(this).parents('.sidebar_filters_area').find('.inner_filters_area').addClass('open_mobile_filter');
    $('body').addClass('scroll_off');
});
}

// Mobile filter hide
if($('.cross_filter').length)
{
$('body').on('click', '.cross_filter', function(){
    $(this).parents('.sidebar_filters_area').find('.inner_filters_area.open_mobile_filter').removeClass('open_mobile_filter');
    $('body').removeClass('scroll_off');
});
}


if($('#banner_slide').length)
{
    $('#banner_slide').owlCarousel({
        items: 1,
        loop:true,
        autoplay:true,
        autoplayTimeout:5000,
        nav:false,
        navText: ["<i class='fi fi-rr-arrow-left'></i>","<i class='fi fi-rr-arrow-right'></i>"],
        dots:true,
        responsive:{
            0:{
                items:1,
                nav:false,
                dots:false,
            },
            600:{
                items:1,
                nav:false,
                dots:false,
            },
            1070:{
                items:1,
                nav:false,
            },
            1200:{
                items:1
            }
        }
    })
}

if($('#arrvials').length)
{
$('#arrvials').owlCarousel({
    items: 4,
    loop:true,
    margin:30,
    stagePadding:20,
    autoplay:true,
    autoplayTimeout:5000,
    nav:true,
    navText: ["<i class='fal fa-arrow-left'></i>","<i class='fal fa-arrow-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
        },
        578:{
            items:1,
            nav:false,
        },
        600:{
            items:2,
            nav:false,
        },
        1070:{
            items:3,
            nav:false,
        },
        1200:{
            items:4
        }
    }
})
}

if($('#pairs').length)
{
$('#pairs').owlCarousel({
    items: 4,
    loop:true,
    margin:30,
    stagePadding:20,
    autoplay:true,
    autoplayTimeout:5000,
    nav:true,
    navText: ["<i class='fal fa-arrow-left'></i>","<i class='fal fa-arrow-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
        },
        578:{
            items:1,
            nav:false,
        },
        600:{
            items:2,
            nav:false,
        },
        1070:{
            items:3,
            nav:false,
        },
        1200:{
            items:4
        }
    }
})
}

if($('#sample-item').length)
{
$('#sample-item').owlCarousel({
    items: 1,
    loop:true,
    margin:0,
    stagePadding:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:true,
    navText: ["<i class='fal fa-arrow-left'></i>","<i class='fal fa-arrow-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
        },
        578:{
            items:1,
            nav:false,
        },
        600:{
            items:1,
            nav:false,
        },
        768:{
            items:1,
            nav:false,
        },
        1024:{
            items:1,
        },
        1200:{
            items:1
        }
    }
})
}


if($('#testimonial').length)
{
$('#testimonial').owlCarousel({
    center: true,
    loop:true,
    margin:50,
    stagePadding: false,
    autoplay:true,
    autoplayTimeout:5000,
    nav:true,
    navText: ["<i class='fal fa-arrow-left'></i>","<i class='fal fa-arrow-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
        },
        600:{
            items:2,
            nav:false,
        },
        1070:{
            items:3,
            nav:false,
        },
        1200:{
            items:3
        }
    }
})
}



$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 20) {
        $(".header-main-section").addClass("shadow");
    }
    else
    {
        $(".header-main-section").removeClass("shadow");
    }
}); //missing );


// Slide tags from right button on car listing top tags
// $('#right-button').click(function() {
//     event.preventDefault();
//     $('.search_tags .list-inline').animate({
//         scrollLeft: "+=100px"
//     }, "slow");
// });

// Slide tags from left button on car listing top tags
// $('#left-button').click(function() {
//     event.preventDefault();
//     $('.search_tags .list-inline').animate({
//         scrollLeft: "-=100px"
//     }, "slow");
// });

// Height match columns
$(document).ready(function () {
    $('.match_height_col').matchHeight();
    $('.match_height_txt').matchHeight();
});

// Scrolls to the selected menu item on the page
// $(function() {
//     var getUrl = window.location.href;
//     getLastName = getUrl.split("#");
//     lastName = getLastName[getLastName.length-1];

//     $('html, body').animate({
//         scrollTop: $('#' + lastName).offset().top - 120
//     }, 1000);
// });


// Calender
// $('#el_calender').datepicker({
//     inline: true,
//     todayHighlight: true,
//     //endDate: "dateToday",
//     format: 'dd/mm/yyyy',
//     autoclose: false
// });

// From Date
// $('#from_date').datepicker({
//     autoclose: false,
//     container: '#from_date_box',
//     todayHighlight: true,
//     orientation: 'auto',
//     //endDate: "dateToday",
//     format: 'dd/mm/yyyy'
// });

// To Date
// $('#to_date').datepicker({
//     autoclose: false,
//     container: '#to_date_box',
//     todayHighlight: true,
//     orientation: 'auto',
//     //endDate: "dateToday",
//     format: 'dd/mm/yyyy'
// });

// Start Date
// $('#start_date').datepicker({
//     autoclose: false,
//     container: '#start_date_box',
//     todayHighlight: true,
//     orientation: 'auto',
//     //endDate: "dateToday",
//     format: 'dd/mm/yyyy'
// });

// $('#new_date').datepicker({
//     autoclose: false,
//     container: '#new_date_box',
//     todayHighlight: true,
//     orientation: 'auto',
//     //endDate: "dateToday",
//     format: 'dd/mm/yyyy'
// });

// End Date
// $('#end_date').datepicker({
//     autoclose: false,
//     container: '#end_date_box',
//     todayHighlight: true,
//     orientation: 'auto',
//     //endDate: "dateToday",
//     format: 'dd/mm/yyyy'
// });


// $("#count-down").TimeCircles({
//     circle_bg_color: "#2E5663",
//     use_background: true,
//     bg_width: 1.0,
//     fg_width: 0.02,
//     time: {
//         Days: {
//             color: "#f00"
//         },
//         Hours: {
//             color: "#f00"
//         },
//         Minutes: {
//             color: "#f00"
//         },
//         Seconds: {
//             color: "#f00"
//         }
//     }
// });


let count = document.querySelectorAll(".count")
let arr = Array.from(count)



arr.map(function(item){
  let startnumber = 0

  function counterup(){
  startnumber++
  item.innerHTML= startnumber
   
  if(startnumber == item.dataset.number){
      clearInterval(stop)
  }
}

let stop =setInterval(function(){
  counterup()
},1)

})



  