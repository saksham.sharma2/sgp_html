<!-- ==== Header === -->
<?php include('common/header.php') ?>

<div class="error_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="error_img">
                    <img src="images/404.png" alt="..." />
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?> 