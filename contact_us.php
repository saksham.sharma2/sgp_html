<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section top-space" style="background-image: url('images/breadcame.png');">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>CONTACT US</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->



<section class="contact-wrap">
    <div class="container">
        <div class="cont-bck-main">
            <div class="cont-bck">
                <div class="row">
                    <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <div class="query-side">
                            <div class="query-title-head-line">
                                <h2>Contact us </h2>
                                <p>You can communicate with us in a variety of ways:</p>
                            </div>
                            <div class="bloom-contact-query">
                                <ul>
                                    <li>
                                        <div class="left">
                                            <div class="icon">
                                                <i class="fi fi-rr-marker"></i>
                                            </div>
                                        </div>  
                                        <div class="right">
                                            <a href="javascript:;">
                                                <p>
                                                    B-X-349, Kucha Kaka Ram, Rehmatulla Road, Opp. M.K. Finishers, Ludhiana - 141008. (Pb.) INDIA 
                                                </p>
                                            </a>
                                        </div>  
                                    </li>
                                    <li>
                                        <div class="left">
                                            <div class="icon">
                                                <i class="fi fi-rr-phone-call"></i>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <a href="tel:+1236478636">
                                                <p>
                                                +91-0161-4501269
                                                </p>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="second">
                                        <div class="left">
                                            <div class="icon">
                                                <img src="images/smartphone.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="right">
                                            <a href="tel:+9872000269">
                                                <p>
                                                +91-98720-00269 , 
                                                </p>
                                            </a>
                                            <a href="tel:+9779540228">
                                                <p>+91-97795-40228</p></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="left">
                                            <div class="icon">
                                                <i class="fi fi-rr-at"></i>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <a href="mailto:info@uniform-inc.com">
                                            <p>
                                                info@uniform-inc.com
                                                </p>
                                            </a>
                                            <a href="mailto:info@capnhats.com">
                                                <p>info@capnhats.com</p>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                         </div>
                    </div>
                    <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 ">
                        <div class="query-form">
                            <form class="row">
                                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label for=""> <img src="images/user1.png"  alt="..." /> First Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Enter your first name">
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label for=""><img src="images/user1.png"  alt="..." />Last Name</label>
                                        <input type="text" class="form-control" id="lname" placeholder="Enter your last name">
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for=""><img src="images/atemail.png"  alt="..." />Email</label>
                                        <input type="email" class="form-control" id="eamil" placeholder=" Enter your email address">
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for=""><img src="images/smartphone.png"  alt="..." />Cell number</label>
                                        <input type="number" class="form-control" id="cell" placeholder=" Enter your cell number">
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for=""><img src="images/mail.png"  alt="..." />Message</label>
                                        <textarea name="" class="form-control" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="subiter float-end">
                                        <button type="submit" class="btn btn-primary-1">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="map">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="locat">
                    <h2>
                        We are Here
                    </h2>
                    <div class="icon">
                        <i class="fal fa-map-pin"></i>
                    </div>
                    <div class="loct">
                        <p>
                            Ludhiana - 141008. (Pb.) INDIA
                        </p>
                    </div>
                    <div class="get">
                        <a href="javascript:;" class="btn btn-primary-1">Get Direction</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?> 