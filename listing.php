<!-- ==== Header === -->
<?php include('common/header.php') ?>


<!-- ==== Listing Section ==== -->
<section class="restaurent_listing_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="restaurent_areas">
                    <div class="row">
                        <?php include('view/listing/sidebar.php'); ?>
                        <?php include('view/listing/restaurant.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>