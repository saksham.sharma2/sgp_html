<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section top-space" style="background-image: url('images/breadcame.png');">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area el text-center">
                <h2>ABOUT US</h2>
                <p>We are Mfrs. & Exporters of Uniform & Uniform Articles on Military, Police, Navy, Air-Force, School & Private Securities.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->
<div class="about_mains">
    <section class="about_us_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="about_start">
                        <div class="row align-items-center">
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="img_area">
                                    <img src="images/Rectangle-120.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-6 col-sm-12 col-12">
                                <div class="content">
                                    <p>
                                    SGP & COMPANY was raised in the early 1960s by S.Sajjan Singh, who had wealth of experience in this field. He laid the stepping stone and guided this organization using the principles of service and sincerity. We originally started supplying our expertise and experience into Uniform Articles of Police, Military, Air- Force, Navy, Schools and Private Securities are enabled to offer better and efficient service to the customers.
                                    </p>
                                    <p>
                                    Passing down to his third generation, at present our organization is handled by the chief administrative, MR. Gurpreet Singh, who has proficient correspondence with customers in more than 30 countries. We believe in constant efforts that ushers our company towards the route of success, thus we are involved in manufacturing and supplying a range of best quality uniform articles.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="we_focus_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="focus_wrap">
                        <div class="content text-center">
                            <h2>We Focus</h2>
                            <p>
                                Our goal is to provide the best value service, dependability and unsurpassed quality in the designs, fashion, fabrics and craftsmanship in uniform articles manufactured.This commitment of ours is easy to see in our high quality products.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="we_deviler_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="focus_wrap">
                        <div class="content text-center">
                            <h2>We Deviler</h2>
                            <p>
                                We supply wide range of uniform products used by Police, Armed forces and by many Government and non-government institutions. All the products are manufactured according to the proper guidelines of our design team.
                            </p>
                        </div>
                        <div class="marks">
                            <ul>
                                <li>
                                    <div class="icon_wrap">
                                        <img src="images/Rectangle124.png" alt="..." />
                                    </div>
                                    <div class="label">
                                        <p>
                                            100% Guranteed
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon_wrap">
                                        <img src="images/Rectangle125.png" alt="..." />
                                    </div>
                                    <div class="label">
                                        <p>
                                            24/7 Support
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="counter">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="conting">
                        <div id="counter">
                            <div class="item">
                            <h1 class="count" data-number="12" ></h1>
                            <h3 class="text">Lorem Ipsum</h3>
                            </div>
                            <div class="item">
                            <h1 class="count" data-number="8" ></h1>
                            <h3 class="text">Dor sit</h3>
                            </div>
                            <div class="item">
                            <h1 class="count" data-number="45" ></h1>
                            <h3 class="text">Ipsum</h3>
                            </div>
                            <div class="item">
                            <h1 class="count" data-number="500">+</h1>
                            <h3 class="text">Lorem sit</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="contact_us">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="focus_wrap">
                    <div class="content">
                        <h2>Contact us </h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ullamcorper neque quis mi luctus,
                        </p>
                        <div class="contact-bt float-end">
                            <a href="contact_us.php" class="btn btn-primary-1">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>