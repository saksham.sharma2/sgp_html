<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section top-space" style="background-image: url('images/breadcame.png');">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>FAQ</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->



<section class="faq_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="faq_detail_area">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Nullam interdum lorem ipsum sit dor aliquam lobortis dolor 
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show"
                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Lorem ipsum sit dor aliquam lobortis dolor 
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree">
                                    Interdum lorem ipsum sit dor aliquam lobortis dolor 
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse"
                                aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading4">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    Ipsum sit dor aliquam lobortis dolor 
                                </button>
                            </h2>
                            <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="heading4"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading5">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                    Nullam interdum lorem ipsum sit dor aliquam lobortis dolor 
                                </button>
                            </h2>
                            <div id="collapse5" class="accordion-collapse collapse" aria-labelledby="heading5"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                    Sit dor aliquam lobortis dolor 
                                </button>
                            </h2>
                            <div id="collapse6" class="accordion-collapse collapse" aria-labelledby="heading6"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>